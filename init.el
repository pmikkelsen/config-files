;; My emacs config file
;; This file (or a symlink to it) should be placed in ~/.emacs.d/

;; First, disable some things
(setq inhibit-splash-screen t)
(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

;; Set the default buffer to an org-mode buffer
(setq initial-major-mode 'org-mode)
(setq initial-scratch-message "")

;; Enable line numbers when in text or prog modes
(add-hook 'prog-mode-hook 'display-line-numbers-mode)
(add-hook 'text-mode-hook 'display-line-numbers-mode)

;; Set the theme and the font
(load-theme 'atom-one-dark t)
(set-frame-font "Inconsolata-16" t t)

;; Save all backup files in a directory instead of putting them everywhere
(setq backup-directory-alist `(("." . "~/.emacs.d/autosaves")))

;; Use ido-mode for command completion
(use-package ido
  :config
  (ido-mode t))

;; Use paredit mode for better lisp editing
(use-package paredit
  :hook ((emacs-lisp-mode . enable-paredit-mode)
	 (lisp-mode       . enable-paredit-mode)
	 (scheme-mode     . enable-paredit-mode)))

;; Use magit for git
(use-package magit
  :bind (("C-x g"   . magit-status)
	 ("C-x M-g" . magit-dispatch-popup)))

;; Use company-mode for auto-completion
(use-package company
  :config
  (global-company-mode))

;; Use SLIME for common lisp
(use-package slime
  :config
  (setq inferior-lisp-program "~/.guix-profile/bin/sbcl")
  (slime-setup '(slime-fancy slime-company)))

;; For dockerfiles
(use-package dockerfile-mode)

;; For yml files
(use-package yaml-mode)

;; End of my config
